
const Koa = require('koa')
const router = require('@koa/router')()
const app = module.exports = new Koa()

// function render(content) {}
const render = (content, color) => {
	return `
		<div style="display: flex; justify-content: center;">
			<h1 style="color:${color};">${content}</h1>
		</div>
	`
}

const home = async (ctx) => {
  ctx.body = render('Home page', 'black')
}

const one = async (ctx) => {
  ctx.body = render('Page: 1', 'red')
}

const two = async (ctx) => {
	ctx.body = render('Page: 2', 'blue')
}

router.get('/', home)
router.get('/one', one)
router.get('/two', two)

app.use(router.routes())

console.log('OK')
app.listen(3000)